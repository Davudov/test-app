package com.example.onlinedb

import android.app.Application
import com.example.onlinedb.network.AppApi
import com.example.onlinedb.repository.DataRepo
import com.example.onlinedb.repository.LoginRepo
import com.example.onlinedb.repository.SharedModule
import com.example.onlinedb.ui.login.LoginViewModel
import com.example.onlinedb.ui.main.data.DataViewModel
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var INSTANCE: App
        val DEFAULT_SHARED_PREFERENCES = "default"
    }

    override fun onCreate() {
        INSTANCE = this
        super.onCreate()
        init()
    }

    private fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initKoin()
    }

    private fun initKoin() {
        val globalModuleDefinition = module {
            single {
                SharedModule(this@App)
            }
            single {
                LoginRepo(AppApi, get())
            }
            single {
                DataRepo(get())
            }
            viewModel {
                LoginViewModel(get())
            }
            viewModel {
                DataViewModel(get())
            }
        }
        startKoin {
            androidContext(this@App)
            modules(listOf(globalModuleDefinition))
        }
    }


}