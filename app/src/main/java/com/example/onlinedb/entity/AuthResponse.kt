package com.example.onlinedb.entity


data class AuthResponse(
   val status: String,
    val code: String
)