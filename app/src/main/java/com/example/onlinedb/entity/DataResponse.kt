package com.example.onlinedb.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


data class DataResponse(
    val data: List<DataEntity>,
    val page: Int,
    val status: String
)

@Parcelize
data class DataEntity(
    val country: String,
    val id: String,
    val lat: Double,
    val lon: Double,
    val name: String
):Parcelable