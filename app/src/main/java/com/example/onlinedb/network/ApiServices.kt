package com.example.onlinedb.network

import com.example.onlinedb.entity.AuthResponse
import com.example.onlinedb.entity.DataResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://www.alarstudios.com/"
private val client = OkHttpClient.Builder().addInterceptor(HeaderInterceptor()).build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .client(client)
    .build()

interface ApiServices {
    @GET("test/auth.cgi")
    fun authAsync(@Query("username") username: String, @Query("password") password: String): Deferred<AuthResponse>

    @GET("test/data.cgi")
    fun getDataAsync(@Query("code") code: String, @Query("p") page: String): Deferred<DataResponse>
}

object AppApi{
    val RETROFIT_SERVICE: ApiServices by lazy { retrofit.create(ApiServices::class.java) }


}