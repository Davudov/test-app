package com.example.onlinedb.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import com.example.onlinedb.entity.AuthResponse
import com.example.onlinedb.entity.DataEntity
import com.example.onlinedb.entity.DataResponse
import com.example.onlinedb.network.AppApi
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import retrofit2.HttpException
import timber.log.Timber
import java.lang.Exception

class DataRepo(private val sharedModule: SharedModule):KoinComponent {

//    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private val _dataInfo: MutableLiveData<Pair<DataResponse?, Status>> =
        MutableLiveData(Pair(null, Status.IDLE))

    val dataInfo :LiveData<Pair<DataResponse?,Status>> = _dataInfo


    suspend fun getData(prevKey:Int?, nextKey: Int,pageNum:Int):PagingSource.LoadResult<Int, DataEntity>{
        _dataInfo.postValue(Pair(null,Status.LOADING))
        val loadRes = try {
            val res =  AppApi.RETROFIT_SERVICE.getDataAsync(sharedModule.getCode().toString(),pageNum.toString()).await()
                if (res.status == "ok") {
                _dataInfo.postValue(Pair(res,Status.SUCCESS))
                PagingSource.LoadResult.Page(res.data, prevKey, nextKey)
            }else {
                _dataInfo.postValue(Pair(null,Status.NOT_SUCCESS))
                PagingSource.LoadResult.Error(Throwable(res.status))
            }
        }catch (e:Exception){
            PagingSource.LoadResult.Error(e)
        }
        _dataInfo.postValue(Pair(null,Status.LOADING_COMPLETED))
        return loadRes
    }

//    suspend fun getDataList(pageNum:Int) {
//        _dataInfo.postValue(Pair(null,Status.LOADING))
//        withContext(ioDispatcher) {
//            try {
//                val res = AppApi.RETROFIT_SERVICE.getDataAsync(sharedModule.getCode().toString(),pageNum.toString()).await()
//                if (res.status == "ok") {
//                    _dataInfo.postValue(Pair(res, Status.SUCCESS))
//                } else _dataInfo.postValue(Pair(res, Status.NOT_SUCCESS))
//            } catch (e: HttpException) {
//                Timber.d(e, "Unable to sign in. Http exception")
//                e.printStackTrace()
//                _dataInfo.postValue(Pair(null, Status.NETWORK_ERROR))
//            } catch (e: Exception) {
//                e.printStackTrace()
//                _dataInfo.postValue(Pair(null, Status.ERROR))
//                Timber.e(e, "Unable to signup")
//            }
//        }
//    }

}