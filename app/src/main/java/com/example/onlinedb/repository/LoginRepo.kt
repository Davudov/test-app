package com.example.onlinedb.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.onlinedb.entity.AuthResponse
import com.example.onlinedb.network.AppApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import retrofit2.HttpException
import timber.log.Timber
import java.lang.Exception

class LoginRepo(
    private val api: AppApi,
    private val sharedModule: SharedModule
) : KoinComponent {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
    private val _loginInfo: MutableLiveData<Pair<AuthResponse?, Status>> =
        MutableLiveData(Pair(null, Status.IDLE))

   val loginInfo:LiveData<Pair<AuthResponse?, Status>> = _loginInfo


    suspend fun login(username: String, password: String) {
        _loginInfo.postValue(Pair(null,Status.LOADING))
        withContext(ioDispatcher) {
            try {
                val res = api.RETROFIT_SERVICE.authAsync(username, password).await()
                if (res.status == "ok") {
                    sharedModule.onLogin(res.code)
                    _loginInfo.postValue(Pair(res, Status.SUCCESS))
                } else {
                    _loginInfo.postValue(Pair(res, Status.NOT_SUCCESS))
                }
            } catch (e: HttpException) {
                Timber.d(e, "Unable to sign in. Http exception")
                e.printStackTrace()
                _loginInfo.postValue(Pair(null, Status.NETWORK_ERROR))
            } catch (e: Exception) {
                e.printStackTrace()
                _loginInfo.postValue(Pair(null, Status.ERROR))
                Timber.e(e, "Unable to signup")
            }
        }
    }
}