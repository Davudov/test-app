package com.example.onlinedb.repository

import android.content.Context
import com.example.onlinedb.App
import timber.log.Timber

class SharedModule(app: App) {
    private val PREFERENCE_ITEM_CODE_KEY = "preference_item_cede"
    private val PREFERENCE_ITEM_ISUSER_LOGGED_IN_KEY = "preference_item_is.user.logged.in"
    private val sharedPreferences =
        app.getSharedPreferences(App.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)

    fun onLogin(code: String) {
        saveCode(code)
        sharedPreferences.edit().putBoolean(PREFERENCE_ITEM_ISUSER_LOGGED_IN_KEY, true).apply()
    }

    fun logOut() {
        sharedPreferences.edit().putBoolean(PREFERENCE_ITEM_ISUSER_LOGGED_IN_KEY, false).apply()
    }

    fun isUserLoggedIn() = sharedPreferences.getBoolean(PREFERENCE_ITEM_ISUSER_LOGGED_IN_KEY,false)

    private fun saveCode(code: String) {
        sharedPreferences.edit().putString(PREFERENCE_ITEM_CODE_KEY, code).apply()
        Timber.d("code - ${getCode()}")
    }

     fun getCode() = sharedPreferences.getString(PREFERENCE_ITEM_CODE_KEY, null)

}