package com.example.onlinedb.repository

enum class Status() { LOADING, LOADING_COMPLETED, ERROR, NETWORK_ERROR, IDLE, SUCCESS, NOT_SUCCESS }
