package com.example.onlinedb.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.onlinedb.repository.SharedModule
import com.example.onlinedb.ui.login.LoginActivity
import com.example.onlinedb.ui.main.MainActivity
import org.koin.android.ext.android.inject

class SplashActivity : Activity() {
    private val profileRepo: SharedModule by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent =
            if (profileRepo.isUserLoggedIn())
                Intent(this, MainActivity::class.java)
            else Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}