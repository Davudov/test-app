package com.example.onlinedb.ui.login

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.onlinedb.R
import com.example.onlinedb.databinding.LayoutLoginBinding
import com.example.onlinedb.repository.Status
import org.koin.android.ext.android.inject
import com.example.onlinedb.ui.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            androidx.databinding.DataBindingUtil.setContentView<LayoutLoginBinding>(
                this,
                R.layout.layout_login
            )
        binding.lifecycleOwner = this
        binding.viewModel = loginViewModel
        loginViewModel.loginInfo.observe(this, {
            when (it.second) {
                Status.SUCCESS -> {
                    startActivity(android.content.Intent(this, MainActivity::class.java))
                    finish()
                }
                Status.NOT_SUCCESS->{
                    Toast.makeText(this,"Auth Failed", Toast.LENGTH_LONG).show()
                }
                Status.NETWORK_ERROR ->{
                    Toast.makeText(this,"Network error", Toast.LENGTH_LONG).show()
                }
                else->{Timber.d("Login error")}
            }
            binding.loginBtn.setOnClickListener {loginViewModel.login(binding.usernameEt.text.toString(),binding.passwordEt.text.toString()) }
        })


    }

}