package com.example.onlinedb.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.onlinedb.repository.LoginRepo
import com.example.onlinedb.repository.Status
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import timber.log.Timber

class LoginViewModel(
    private val loginRepo: LoginRepo,
    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main
) : ViewModel(), KoinComponent {

    private val viewModelScope = CoroutineScope(Dispatchers.Main)
    private val isLoading = MutableLiveData(false)
    val loginInfo = loginRepo.loginInfo

    val progressVisible: LiveData<Boolean> = Transformations.switchMap(loginInfo) {
        isLoading.postValue(it.second == Status.LOADING)
        isLoading
    }


    fun login(username: String, password: String) {
        viewModelScope.launch {
            withContext(mainDispatcher) {
                loginRepo.login(username, password)
            }
        }
    }
}