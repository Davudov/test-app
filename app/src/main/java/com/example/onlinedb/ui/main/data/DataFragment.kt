package com.example.onlinedb.ui.main.data

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.map
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.onlinedb.databinding.FragmentDataBinding
import com.example.onlinedb.ui.login.LoginViewModel
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.KoinComponent
import timber.log.Timber

class DataFragment : Fragment(),KoinComponent {
    private lateinit var binding: FragmentDataBinding
    private lateinit var adapter: DataListAdapter
    private val dataViewModel: DataViewModel by viewModel()
    private  val navController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDataBinding.inflate(inflater)
        binding.viewModel = dataViewModel
        binding.lifecycleOwner = this
        adapter = DataListAdapter(DataClickListener {item->
            navController.navigate(DataFragmentDirections.actionMainFragmentToMapFragment(item))
            true
        })
        initDataObservers()
        binding.dataList.adapter = adapter

        val decoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)

        binding.dataList.addItemDecoration(decoration)
        return binding.root
    }

    private fun initDataObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            dataViewModel.getDataList().observe(viewLifecycleOwner, { list ->
                adapter.submitData(lifecycle, list.map { DataItem.DataFilled(it) })
            })
        }
    }
}