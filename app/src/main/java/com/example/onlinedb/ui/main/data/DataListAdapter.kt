package com.example.onlinedb.ui.main.data

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.onlinedb.R
import com.example.onlinedb.databinding.ListItemDataViewBinding
import com.example.onlinedb.entity.DataEntity

private const val DATA_TYPE_FILLED = 0

class DataListAdapter(private val dataClickListener: DataClickListener) :
    PagingDataAdapter<DataItem, RecyclerView.ViewHolder>(DataDiffCallback()) {


    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.DataFilled -> DATA_TYPE_FILLED
            else -> throw ClassCastException("Unknown data type")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            DATA_TYPE_FILLED -> {
                FilledDataViewHolder.from(parent)
            }
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FilledDataViewHolder -> {
                holder.bind(
                    clickListener = dataClickListener,
                    getItem(position) as DataItem.DataFilled
                )
            }
        }
    }


    class FilledDataViewHolder(
        private val binding: ListItemDataViewBinding, val parent: ViewGroup
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            clickListener: DataClickListener,
            model: DataItem.DataFilled,
        ) {
            val item = model.model
            binding.item = item
            binding.root.setOnClickListener {
               clickListener.onClick(item)
            }

           val url = "https://media.nationalgeographic.org/assets/photos/000/285/28${500+absoluteAdapterPosition}.jpg"

            Glide.with(parent.context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(binding.imageView);

            binding.executePendingBindings()
        }


        companion object {

            fun from(parent: ViewGroup): FilledDataViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemDataViewBinding.inflate(layoutInflater, parent, false)
                return FilledDataViewHolder(binding, parent)
            }
        }
    }
}


class DataDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(
        oldItem: DataItem,
        newItem: DataItem
    ): Boolean {
        return oldItem === newItem
    }

    override fun areContentsTheSame(
        oldItem: DataItem,
        newItem: DataItem
    ): Boolean {
        return oldItem == newItem
    }
}


class DataClickListener(
    val clickListener: (item:DataEntity) -> Boolean,
) {
    fun onClick(item: DataEntity):  Boolean =
        clickListener(item)
}

sealed class DataItem() {
    data class DataFilled(val model: DataEntity) : DataItem()
}