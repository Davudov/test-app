package com.example.onlinedb.ui.main.data

import androidx.lifecycle.*
import androidx.paging.*
import com.example.onlinedb.entity.DataEntity
import com.example.onlinedb.repository.DataRepo
import com.example.onlinedb.repository.Status
import com.example.onlinedb.ui.paging.ListDataSource
import org.koin.core.KoinComponent

class DataViewModel(private val dataRepo: DataRepo) : ViewModel(),KoinComponent {
    private val isLoading = MutableLiveData(false)

    val progressVisible: LiveData<Boolean> = Transformations.switchMap(dataRepo.dataInfo) {
        isLoading.postValue(it.second == Status.LOADING)
        isLoading
    }

    fun getDataList(): LiveData<PagingData<DataEntity>> {
        val config = PagingConfig(
            pageSize = 20
        )
        val dataSource = ListDataSource(dataRepo)
        val pager = Pager(config = config, pagingSourceFactory = { dataSource })
        return pager.liveData.cachedIn(viewModelScope)//persist the data beyond viewModel configuration changes
    }


}