package com.example.onlinedb.ui.main.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.onlinedb.R
import com.example.onlinedb.databinding.FragmentMapBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentMapBinding

    private val args by navArgs<MapFragmentArgs>()

    private val ZOOM = 17.0f


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapBinding.inflate(inflater)

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        initView()

        return binding.root
    }

    private fun initView() {
        binding.placeId.text = getString(R.string.place_id,args.data.id)
        binding.placeName.text = getString(R.string.place_name,args.data.name)
        binding.placeCountry.text = getString(R.string.place_country,args.data.country)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        val latLng = LatLng(args.data.lat, args.data.lon)
        googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title(args.data.name)
        )

        googleMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                latLng,
                ZOOM
            )
        )
    }
}