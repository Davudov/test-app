package com.example.onlinedb.ui.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.onlinedb.entity.DataEntity
import com.example.onlinedb.network.AppApi
import com.example.onlinedb.repository.DataRepo
import timber.log.Timber
import java.lang.Exception

class ListDataSource(private val dataRepo: DataRepo) : PagingSource<Int, DataEntity>() {

    private val INITIAL_LOAD_SIZE = 0

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, DataEntity> {
        val position = params.key ?: INITIAL_LOAD_SIZE
        val prevKey = null
        val nextKey = position + 1
        return try {
            dataRepo.getData(prevKey,nextKey,position)
        }catch (e:Exception){
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, DataEntity>): Int? {
        return state.anchorPosition
    }
}